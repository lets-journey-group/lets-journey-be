﻿namespace LetsJourney.BE.Business.Tests;

public abstract class TestBase
{
    protected const string AddressStreet = "test street";
    protected const string AddressStreet2 = "test street";
    protected const decimal MoneyValue = 5m;
    protected readonly Passenger Passenger = new();
    protected readonly Driver Driver = new();
    protected readonly Money Money = new(MoneyValue);
    protected readonly Address Address = new(AddressStreet);
    protected readonly Address Address2 = new(AddressStreet2);
}
