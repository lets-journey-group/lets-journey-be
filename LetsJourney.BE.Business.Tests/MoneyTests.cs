﻿using FluentAssertions;

namespace LetsJourney.BE.Business.Tests;

public class MoneyTests : TestBase
{
    #region Constructor

    [Fact]
    public void Constructor_WithValidParameters_ShouldCreateNewOne()
    {
        //Arrange
        const decimal value = 25;

        //Act
        Money money = new(value);

        //Assert
        money.Should().NotBeNull();
        money.Value.Should().Be(value);
    }

    #endregion

    #region CompareTo

    [Fact]
    public void CompareTo_WithEqualsParameters_ShouldReturnZero()
    {
        //Arrange
        Money first = new(5);
        Money second = new(5);

        //Act
        var result = first.CompareTo(second);

        //Assert
        result.Should().Be(0);
    }

    [Fact]
    public void CompareTo_WithGreaterSecondParameter_ShouldReturnNegativeOne()
    {
        //Arrange
        Money first = new(5);
        Money second = new(10);

        //Act
        var result = first.CompareTo(second);

        //Assert
        result.Should().Be(-1);
    }

    [Fact]
    public void CompareTo_WithLesserSecondParameter_ShouldReturnPositiveOne()
    {
        //Arrange
        Money first = new(10);
        Money second = new(5);

        //Act
        var result = first.CompareTo(second);

        //Assert
        result.Should().Be(1);
    }

    [Fact]
    public void CompareTo_WithNullSecondParameter_ShouldThrowException()
    {
        //Arrange
        Money first = new(5);
        Money second = null;

        //Act
        Func<int> func = () => first.CompareTo(second);

        //Assert
        func.Should().Throw<ArgumentNullException>();
    }

    #endregion
}
