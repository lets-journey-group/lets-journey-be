using FluentAssertions;
using Moq;

namespace LetsJourney.BE.Business.Tests;

public class AuctionTests : TestBase
{
    #region Factory

    [Fact]
    public void Factory_WithValidParameters_ShouldCreateNewOne()
    {
        //Act
        Auction auction = Auction.Create(Passenger, AddressStreet, AddressStreet2);

        //Assert
        auction.Should().NotBeNull();
        auction.Passenger.Should().Be(Passenger);
        auction.From.Should().Be(Address);
        auction.To.Should().Be(Address2);
        auction.CandidatesByDriverId.Should().NotBeNull();
    }

    #endregion

    #region Constructor

    [Fact]
    public void Constructor_WithValidParameters_ShouldCreateNewOne()
    {
        //Act
        Auction auction = new(Passenger, Address, Address2);

        //Assert
        auction.Should().NotBeNull();
        auction.Passenger.Should().Be(Passenger);
        auction.From.Should().Be(Address);
        auction.To.Should().Be(Address2);
        auction.CandidatesByDriverId.Should().NotBeNull();
    }

    [Fact]
    public void Constructor_WithNullParameters_ShouldThrowException()
    {
        //Act
        Func<Auction> func1 = () => new Auction(null, Address, Address);
        Func<Auction> func2 = () => new Auction(Passenger, null, Address);
        Func<Auction> func3 = () => new Auction(Passenger, Address, null);

        //Assert
        func1.Should().Throw<ArgumentNullException>();
        func2.Should().Throw<ArgumentNullException>();
        func3.Should().Throw<ArgumentNullException>();
    }

    #endregion

    #region AddCandidate

    [Fact]
    public void AddCandidate_WithValidCandidate_ShouldBeAdded()
    {
        //Arrange
        var auction = new Auction(Passenger, Address, Address2);

        //Act
        auction.AddCandidate(Driver, MoneyValue);

        //Assert
        auction.CandidatesByDriverId.Should().HaveCount(1);
    }

    [Fact]
    public void AddCandidate_WithNullParameters_ShouldThrowException()
    {
        //Arrange
        var auction = new Auction(Passenger, Address, Address2);

        //Act
        Action act1 = () => auction.AddCandidate(null, MoneyValue);

        //Assert
        act1.Should().Throw<ArgumentNullException>();
    }

    [Fact]
    public void AddCandidate_WithAlreadyExsistCandidate_ShouldReject()
    {
        //Arrange
        decimal secondMoney = 1m;
        var auction = new Auction(Passenger, Address, Address2);

        auction.AddCandidate(Driver, MoneyValue);

        //Act
        Action act = () => auction.AddCandidate(Driver, secondMoney);

        //Assert
        act.Should().Throw<InvalidOperationException>();
    }

    #endregion

    #region CreateJourney

    [Fact]
    public void CreateJourney_WithValidCandidate_ShouldCreate()
    {
        //Arrange
        Auction auction = new(Passenger, Address, Address2);
        auction.AddCandidate(Driver, MoneyValue);

        //Act
        var resultJourney = auction.CreateJourney(Driver.Id);

        //Assert
        resultJourney.Should().NotBeNull();
        resultJourney.Driver.Should().Be(Driver);
        resultJourney.Cost.Should().Be(Money);
        resultJourney.From.Should().Be(Address);
        resultJourney.To.Should().Be(Address2);
    }

    [Fact]
    public void CreateJourney_WithoutAddedCandidate_ShouldReject()
    {
        //Arrange
        Auction auction = new(Passenger, Address, Address2);

        //Act
        Func<Journey> func = () => auction.CreateJourney(Driver.Id);

        //Assert
        func.Should().Throw<InvalidOperationException>();
    }

    #endregion

    #region CreateJourneyWithChipestCandidate

    [Fact]
    public void CreateJourneyWithChipestCandidate_Valid_ShouldCreate()
    {
        //Arrange
        Driver driver = new();
        Driver driverChipest = Mock.Of<Driver>(d => d.Id == Guid.NewGuid());
        decimal moneyValueChipest = 2m;
        Money moneyChipest = new(moneyValueChipest);

        Auction auction = new(Passenger, Address, Address2);

        auction.AddCandidate(driver, MoneyValue);
        auction.AddCandidate(driverChipest, moneyValueChipest);

        //Act
        var resultJourney = auction.CreateJourneyWithChipestCandidate();

        //Assert
        resultJourney.Should().NotBeNull();
        resultJourney.Driver.Should().Be(driverChipest);
        resultJourney.Cost.Should().Be(moneyChipest);
    }

    [Fact]
    public void CreateJourneyWithChipestCandidate_WithoutCandidats_ShouldThrowException()
    {
        //Arrange
        Auction auction = new(Passenger, Address, Address2);

        //Act
        Action act = () => auction.CreateJourneyWithChipestCandidate();

        //Assert
        act.Should().Throw<InvalidOperationException>();
    }

    [Fact]
    public void CreateJourneyWithChipestCandidate_WithTwoEqualsCandidats_ShouldCreateWithFirstDriver()
    {
        //Arrange
        Driver secondDriver = Mock.Of<Driver>(d => d.Id == Guid.NewGuid());

        Auction auction = new(Passenger, Address, Address2);
        auction.AddCandidate(Driver, MoneyValue);
        auction.AddCandidate(secondDriver, MoneyValue);

        //Act
        var journey = auction.CreateJourneyWithChipestCandidate();

        //Assert
        journey.Should().NotBeNull();
        journey.Driver.Should().Be(Driver);
    }

    #endregion
}