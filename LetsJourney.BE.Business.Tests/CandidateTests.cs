﻿using FluentAssertions;

namespace LetsJourney.BE.Business.Tests;

public class CandidateTests : TestBase
{
    [Fact]
    public void Constructor_WithValidParameters_ShouldCreateNewOne()
    {
        //Act
        Candidate candidate = new(Driver, Money);

        //Assert
        candidate.Should().NotBeNull();
        candidate.Driver.Should().Be(Driver);
        candidate.Cost.Should().Be(Money);
    }

    [Fact]
    public void Constructor_WithNullParameters_ShouldThrowException()
    {
        //Act
        Func<Candidate> func1 = () => new Candidate(null, Money);
        Func<Candidate> func2 = () => new Candidate(Driver, null);

        //Assert
        func1.Should().Throw<ArgumentNullException>();
        func2.Should().Throw<ArgumentNullException>();
    }
}
