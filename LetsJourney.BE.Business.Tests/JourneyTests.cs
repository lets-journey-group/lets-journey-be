﻿using FluentAssertions;

namespace LetsJourney.BE.Business.Tests;

public class JourneyTests : TestBase
{
    #region Constructor

    [Fact]
    public void Constructor_WithValidParameters_ShouldCreateNewOne()
    {
        //Act
        Journey journey = new(Passenger, Driver, Address, Address2, Money);

        //Assert
        journey.Should().NotBeNull();
        journey.Passenger.Should().Be(Passenger);
        journey.Driver.Should().Be(Driver);
        journey.From.Should().Be(Address);
        journey.To.Should().Be(Address2);
        journey.Cost.Should().Be(Money);
    }

    [Fact]
    public void Constructor_WithNullParameters_ShouldThrowException()
    {
        //Act
        Func<Journey> func1 = () => new Journey(null, Driver, Address, Address2, Money);
        Func<Journey> func2 = () => new Journey(Passenger, null, Address, Address2, Money);
        Func<Journey> func3 = () => new Journey(Passenger, Driver, null, Address2, Money);
        Func<Journey> func4 = () => new Journey(Passenger, Driver, Address, null, Money);
        Func<Journey> func5 = () => new Journey(Passenger, Driver, Address, Address2, null);

        //Assert
        func1.Should().Throw<ArgumentNullException>();
        func2.Should().Throw<ArgumentNullException>();
        func3.Should().Throw<ArgumentNullException>();
        func4.Should().Throw<ArgumentNullException>();
        func5.Should().Throw<ArgumentNullException>();
    }

    #endregion
}
