﻿using AutoMapper;

namespace LetsJourney.BE.Api.Mapping;

internal static class MapperFactory
{
    private static readonly Mapper mapper;

    static MapperFactory()
    {
        MapperConfiguration config = new(
            cfg =>
            {
                cfg.CreateMap<Business.Auction, DTO.Output.Auction>()
                    .ForMember(dto => dto.PassengerId, s => s.MapFrom(entity => entity.Passenger.Id))
                    .ForMember(dto => dto.CandidateIdCollection, s => s.MapFrom(entity => entity.CandidatesByDriverId.Keys));

                cfg.CreateMap<Business.Address, DTO.Output.Address>();
            });

        mapper = new Mapper(config)
            ?? throw new InvalidOperationException("Mapper not created");
    }

    public static Mapper GetMapper() => mapper;
}
