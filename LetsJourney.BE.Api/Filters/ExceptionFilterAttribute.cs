﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace LetsJourney.BE.Api.Filters;

[AttributeUsage(AttributeTargets.All)]
public class ExceptionFilterAttribute : Attribute, IExceptionFilter
{
    private const string rootNamespace = $"{nameof(LetsJourney)}.{nameof(BE)}";
    private const string business = $"{rootNamespace}.{nameof(Business)}";
    private const string services = $"{rootNamespace}.{nameof(Services)}";
    private const string data = $"{rootNamespace}.{nameof(Data)}";
    private readonly ILogger<ExceptionFilterAttribute> logger;

    public ExceptionFilterAttribute(ILogger<ExceptionFilterAttribute> logger)
    {
        this.logger = logger;
    }

    public void OnException(ExceptionContext context)
    {
        var exception = context.Exception;
        if (!IsExceptionManuallyThrowed(exception))
        {
            return;
        }

        if (exception is InvalidOperationException)
        {
            context.Result = new BadRequestObjectResult(exception.Message);
            logger.LogInformation($"{exception.Message} {{@Exception}}", exception);
        }
    }

    private static bool IsExceptionManuallyThrowed(Exception exception)
    {
        return
            exception.Source is business
            or services
            or data;
    }
}
