﻿using AutoMapper;
using LetsJourney.BE.Api.Mapping;
using LetsJourney.BE.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace LetsJourney.BE.Api.Controllers;

[Route("api/[controller]")]
[ApiController]
public class AuctionsController : ControllerBase
{
    //TODO: refactor to authorization.
    private readonly Guid mockUserGuid = new("FCDE5C9E-69D1-4193-B569-A4143CD78B2D");
    private readonly Guid mockUserGuid2 = new("A208F0A4-0766-441B-B070-D67F53D18E7F");
    private readonly ILogger<AuctionsController> logger;
    private readonly IManageAuction manageAuctionUseCase;
    private readonly IParticipateAuction participateAuctionUseCase;

    private readonly Mapper mapper;

    public AuctionsController(
        ILogger<AuctionsController> logger,
        IManageAuction manageAuction,
        IParticipateAuction participateAuction)
    {
        this.logger = logger;
        this.manageAuctionUseCase = manageAuction;
        this.participateAuctionUseCase = participateAuction;
        mapper = MapperFactory.GetMapper();
    }

    [HttpGet]
    [ProducesResponseType(typeof(DTO.Output.Auction[]), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetAllAsync()
    {
        var resultAuctionCollection = await participateAuctionUseCase.GetAuctionsAsync();

        var mappedAuction = mapper.Map<IEnumerable<DTO.Output.Auction>>(resultAuctionCollection);
        return Ok(mappedAuction);
    }

    [HttpGet("{id}")]
    [ProducesResponseType(typeof(DTO.Output.Auction), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> GetAsync(int id)
    {
        var resultAuction = await participateAuctionUseCase.GetAuctionOrDefaultAsync(id);
        if (resultAuction is null)
        {
            return NotFound();
        }

        var mappedAuction = mapper.Map<DTO.Output.Auction>(resultAuction);
        return Ok(mappedAuction);
    }

    [HttpPost]
    [ProducesResponseType(typeof(DTO.Output.Auction), StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> CreateAsync([FromBody] DTO.Input.Auction auction)
    {
        var resultAuction = await manageAuctionUseCase.CreateAuctionAsync(auction.From, auction.To, mockUserGuid);
        logger.LogInformation("Created auction {@ResultAuction}", resultAuction);

        var mappedAuction = mapper.Map<DTO.Output.Auction>(resultAuction);
        return CreatedAtAction(nameof(GetAsync), new { id = mappedAuction.Id }, mappedAuction);
    }

    [HttpPost("{auctionId}/candidates")]
    [ProducesResponseType(typeof(DTO.Output.Auction), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> AddCandidateAsync(int auctionId, [FromBody][Required] decimal bet)
    {
        await participateAuctionUseCase.GoInAuctionAsync(auctionId, bet, mockUserGuid2);
        var resultAuction = await participateAuctionUseCase.GetAuctionOrDefaultAsync(auctionId);
        if (resultAuction is null)
        {
            return Problem(detail: "Parent auction not found.", statusCode: StatusCodes.Status500InternalServerError);
        }

        var mappedAuction = mapper.Map<DTO.Output.Auction>(resultAuction);
        return Ok(mappedAuction);
    }
}
