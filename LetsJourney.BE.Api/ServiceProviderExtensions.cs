﻿using LetsJourney.BE.Data.Repositories;
using LetsJourney.BE.Services.Gateways.Repositories;
using LetsJourney.BE.Services.Interfaces;
using LetsJourney.BE.Services.UseCases.Driver;
using LetsJourney.BE.Services.UseCases.Passenger;

namespace LetsJourney.BE.Api;

public static class ServiceProviderExtensions
{
    public static void AddUseCaseServices(this IServiceCollection services)
    {
        services.AddScoped<IManageAuction, ManageAuctionUseCase>();
        services.AddScoped<IParticipateAuction, ParticipateAuctionUseCase>();
    }

    public static void AddGatewayServices(this IServiceCollection services)
    {
        services.AddScoped<IAuctionRepository, AuctionRepository>();
        services.AddScoped<IUserRepository, UserRepository>();
    }
}
