﻿namespace LetsJourney.BE.Api.Configuration;

public class ConnectionStrings
{
    public string JourneyDB { get; init; }

    public string Seq { get; init; }
}
