﻿using System.ComponentModel.DataAnnotations;

namespace LetsJourney.BE.Api.DTO.Input;

[Display(Name = $"{nameof(Input)}.{nameof(Auction)}")]
public record Auction
{
    [Required]
    [Display(Name = nameof(From))]
    public required string From { get; init; }

    [Required]
    [Display(Name = nameof(To))]
    public required string To { get; init; }
}
