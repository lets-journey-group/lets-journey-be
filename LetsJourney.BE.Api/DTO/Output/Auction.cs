﻿using System.ComponentModel.DataAnnotations;

namespace LetsJourney.BE.Api.DTO.Output;

[Display(Name = $"{nameof(Output)}.{nameof(Auction)}")]
public record Auction
{
    public int Id { get; init; }

    public Guid PassengerId { get; init; }

    [Required]
    public required Address From { get; init; }

    [Required]
    public required Address To { get; init; }

    public List<string>? CandidateIdCollection { get; init; }
}
