﻿using System.ComponentModel.DataAnnotations;

namespace LetsJourney.BE.Api.DTO.Output;

[Display(Name = $"{nameof(Output)}.{nameof(Address)}")]
public record Address
{
    public required string Street { get; init; }
}
