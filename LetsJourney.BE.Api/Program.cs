using LetsJourney.BE.Api;
using LetsJourney.BE.Api.Configuration;
using LetsJourney.BE.Api.Filters;
using LetsJourney.BE.Data;
using LetsJourney.BE.Services.Gateways;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.EntityFrameworkCore;
using Serilog;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

var appSettings = builder.Configuration.Get<AppSettings>()
    ?? throw new InvalidOperationException("Invalid configuration mapping.");

LogSettings.SetupLogs(appSettings);

// Add services to the container.

builder.Logging
    .ClearProviders()
    .AddSerilog();

builder.Services.AddDbContext<IJourneyContext, JourneyContext>(options =>
{
    options.UseNpgsql(appSettings.ConnectionStrings.JourneyDB);
});

builder.Services.AddControllers(options =>
{
    options.SuppressAsyncSuffixInActionNames = false;
    options.Filters.Add<ExceptionFilterAttribute>();
});

builder.Services.AddRouting(opt => opt.LowercaseUrls = true);
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c =>
{
    c.CustomSchemaIds(x =>
    {
        var attribute = x.GetCustomAttributes<DisplayAttribute>().SingleOrDefault();
        return attribute is null ? x.Name : attribute.Name;
    });
    c.CustomOperationIds(
            description => description.ActionDescriptor is not ControllerActionDescriptor actionDescriptor
                ? null
                : actionDescriptor.ActionName);
});

builder.Services.AddUseCaseServices();
builder.Services.AddGatewayServices();

var app = builder.Build();

// Configure the HTTP request pipeline.

//if (app.Environment.IsDevelopment())
if (true)
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

public partial class Program { }