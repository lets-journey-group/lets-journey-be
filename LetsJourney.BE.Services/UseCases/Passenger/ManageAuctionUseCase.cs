﻿using LetsJourney.BE.Business;
using LetsJourney.BE.Services.Gateways;
using LetsJourney.BE.Services.Gateways.Repositories;
using LetsJourney.BE.Services.Interfaces;

namespace LetsJourney.BE.Services.UseCases.Passenger;

public class ManageAuctionUseCase : UseCaseBase, IManageAuction
{
    private readonly IAuctionRepository auctionRepository;
    private readonly IUserRepository userRepository;

    public ManageAuctionUseCase(
        IAuctionRepository auctionRepository,
        IUserRepository userRepository,
        IJourneyContext journeyContext) : base(journeyContext)
    {
        this.auctionRepository = auctionRepository;
        this.userRepository = userRepository;
    }

    public async Task<Auction> CreateAuctionAsync(string fromStreet, string toStreet, Guid userId)
    {
        var passenger = await userRepository.RetrievePassengerAsync(userId);

        var auction = Auction.Create(passenger, fromStreet, toStreet);

        JourneyContext.Add(auction);
        await JourneyContext.SaveChangesAsync();
        return auction;
    }

    public async Task CreateJourneyWithChipestCandidateAsync(Guid userId)
    {
        var auction = await auctionRepository.RetrieveByPassengerAsync(userId);
        var journey = auction.CreateJourneyWithChipestCandidate();

        JourneyContext.Add(journey);
        await JourneyContext.SaveChangesAsync();
    }
}
