﻿using LetsJourney.BE.Business;
using LetsJourney.BE.Services.Gateways;
using LetsJourney.BE.Services.Gateways.Repositories;
using LetsJourney.BE.Services.Interfaces;

namespace LetsJourney.BE.Services.UseCases.Driver;

public class ParticipateAuctionUseCase : UseCaseBase, IParticipateAuction
{
    private readonly IAuctionRepository auctionRepository;
    private readonly IUserRepository userRepository;

    public ParticipateAuctionUseCase(
        IAuctionRepository auctionRepository,
        IUserRepository driverRepository,
        IJourneyContext journeyContext) : base(journeyContext)
    {
        this.auctionRepository = auctionRepository;
        this.userRepository = driverRepository;
    }

    public async Task<IList<Auction>> GetAuctionsAsync()
    {
        return await auctionRepository.RetrieveAllAsync();
    }

    public async Task<Auction?> GetAuctionOrDefaultAsync(int auctionId)
    {
        return await auctionRepository.RetrieveOrDefaultAsync(auctionId);
    }

    public async Task GoInAuctionAsync(int auctionId, decimal bet, Guid userId)
    {
        var driver = await userRepository.RetrieveDriverAsync(userId);
        var auction = await auctionRepository.RetrieveAsync(auctionId);

        auction.AddCandidate(driver, bet);

        await JourneyContext.SaveChangesAsync();
    }
}
