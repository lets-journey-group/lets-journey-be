﻿using LetsJourney.BE.Services.Gateways;

namespace LetsJourney.BE.Services.UseCases;

public abstract class UseCaseBase
{
    protected IJourneyContext JourneyContext;

    public UseCaseBase(IJourneyContext journeyContext)
    {
        this.JourneyContext = journeyContext;
    }
}
