﻿using LetsJourney.BE.Business;

namespace LetsJourney.BE.Services.Interfaces;

public interface IParticipateAuction
{
    Task<IList<Auction>> GetAuctionsAsync();

    Task<Auction?> GetAuctionOrDefaultAsync(int auctionId);

    Task GoInAuctionAsync(int auctionId, decimal bet, Guid userId);
}
