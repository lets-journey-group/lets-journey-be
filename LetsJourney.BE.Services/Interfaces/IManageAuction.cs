﻿using LetsJourney.BE.Business;

namespace LetsJourney.BE.Services.Interfaces;

public interface IManageAuction
{
    Task<Auction> CreateAuctionAsync(string fromStreet, string toStreet, Guid userId);

    Task CreateJourneyWithChipestCandidateAsync(Guid userId);
}
