﻿using LetsJourney.BE.Business;

namespace LetsJourney.BE.Services.Gateways.Repositories;

public interface IUserRepository
{
    Task<Passenger?> RetrievePassengerOrDefaultAsync(Guid userId);

    Task<Passenger> RetrievePassengerAsync(Guid userId);

    Task<Driver?> RetrieveDriverOrDefaultAsync(Guid userId);

    Task<Driver> RetrieveDriverAsync(Guid userId);
}
