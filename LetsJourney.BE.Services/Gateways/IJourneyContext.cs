﻿namespace LetsJourney.BE.Services.Gateways;

public interface IJourneyContext
{
    void Add<TEntity>(TEntity entity) where TEntity : class;

    Task SaveChangesAsync();
}
