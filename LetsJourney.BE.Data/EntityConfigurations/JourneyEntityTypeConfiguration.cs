﻿using LetsJourney.BE.Business;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LetsJourney.BE.Data.EntityConfigurations;

internal class JourneyEntityTypeConfiguration : IEntityTypeConfiguration<Journey>
{
    public void Configure(EntityTypeBuilder<Journey> journeyConfiguration)
    {
        journeyConfiguration
            .OwnsOne(j => j.From);
        journeyConfiguration
            .OwnsOne(j => j.To);
        journeyConfiguration
            .OwnsOne(j => j.Cost);
    }
}
