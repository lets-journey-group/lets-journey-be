﻿using LetsJourney.BE.Business;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LetsJourney.BE.Data.EntityConfigurations;

internal class AuctionEntityTypeConfiguration : IEntityTypeConfiguration<Auction>
{
    public void Configure(EntityTypeBuilder<Auction> auctionConfiguration)
    {
        auctionConfiguration
            .OwnsOne(a => a.From);
        auctionConfiguration
            .OwnsOne(a => a.To);
        auctionConfiguration
            .OwnsMany<Candidate>(
                ConvertListName(nameof(Candidate)),
                b =>
                {
                    b.HasKey("Id");
                    b.OwnsOne(c => c.Cost);
                    b.Navigation(c => c.Driver).AutoInclude();
                });
    }

    private static string ConvertDictionaryNameToListName(string dictionaryName)
    {
        const string dictionarySeparator = "By";
        return dictionaryName.Split(dictionarySeparator)[0].ToLower();
    }

    private static string ConvertListName(string listName)
    {
        return (listName + "s").ToLower();
    }
}
