﻿namespace LetsJourney.BE.Data;

public abstract class RepositoryBase
{
    protected readonly JourneyContext Context;

    public RepositoryBase(JourneyContext context)
    {
        ArgumentNullException.ThrowIfNull(context);
        this.Context = context;
    }
}
