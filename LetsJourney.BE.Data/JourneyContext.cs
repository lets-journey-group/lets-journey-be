﻿using LetsJourney.BE.Business;
using LetsJourney.BE.Data.EntityConfigurations;
using LetsJourney.BE.Services.Gateways;
using Microsoft.EntityFrameworkCore;

namespace LetsJourney.BE.Data;

public class JourneyContext : DbContext, IJourneyContext
{
    public DbSet<Auction> Auctions { get; set; }

    public DbSet<Driver> Drivers { get; set; }

    public DbSet<Passenger> Passengers { get; set; }

    public DbSet<Journey> Journeys { get; set; }

    public JourneyContext()
    {
        Database.EnsureCreated();
    }

    public JourneyContext(DbContextOptions<JourneyContext> options) : base(options)
    {
        Database.EnsureCreated();
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfiguration(new AuctionEntityTypeConfiguration());
        modelBuilder.ApplyConfiguration(new JourneyEntityTypeConfiguration());
        modelBuilder.ApplyConfiguration(new UserEntityTypeConfiguration());
    }

    void IJourneyContext.Add<TEntity>(TEntity entity)
    {
        base.Add(entity);
    }

    public async Task SaveChangesAsync()
    {
        await base.SaveChangesAsync();
    }
}
