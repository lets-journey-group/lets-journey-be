﻿using LetsJourney.BE.Business;
using LetsJourney.BE.Services.Gateways.Repositories;
using Microsoft.EntityFrameworkCore;

namespace LetsJourney.BE.Data.Repositories;

public class AuctionRepository : RepositoryBase, IAuctionRepository
{
    public AuctionRepository(JourneyContext context) : base(context)
    {
    }

    public async Task<List<Auction>> RetrieveAllAsync()
    {
        return await Context.Auctions
            .Include(a => a.Passenger)
            .ToListAsync();
    }

    public async Task<Auction?> RetrieveOrDefaultAsync(int id)
    {
        return await Context.Auctions
            .Include(a => a.Passenger)
            .SingleOrDefaultAsync(a => a.Id == id);
    }

    public async Task<Auction> RetrieveAsync(int id)
    {
        return await RetrieveOrDefaultAsync(id) ??
            throw new InvalidOperationException($"Auction with id '{id}' not found.");
    }

    public async Task<Auction> RetrieveByPassengerAsync(Guid userId)
    {
        return await Context.Auctions.SingleAsync(a => a.Passenger.Id == userId) ??
            throw new InvalidOperationException($"Auction with passenger id '{userId}' not found.");
    }

    public async Task<Auction> RetrieveByPassengerAsync(Passenger passenger)
    {
        return await Context.Auctions.SingleAsync(a => a.Passenger == passenger) ??
            throw new InvalidOperationException($"Auction with passenger id '{passenger.Id}' not found.");
    }
}
