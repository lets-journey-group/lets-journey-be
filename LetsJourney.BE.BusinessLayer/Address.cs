﻿namespace LetsJourney.BE.Business;

public record Address
{
    #region private constructor
#pragma warning disable CS8618
    private Address()
    {
    }
#pragma warning restore CS8618
    #endregion

    internal Address(string street)
    {
        Street = street;
    }

    public string Street { get; private init; }
}
