﻿namespace LetsJourney.BE.Business;

public class Auction
{
    private readonly List<Candidate> candidates = new();

    public static Auction Create(Passenger passenger, string from, string to)
    {
        var fromAddress = new Address(from);
        var toAddress = new Address(to);
        return new Auction(passenger, fromAddress, toAddress);
    }

    #region private constructor
#pragma warning disable CS8618
    private Auction()
    {
    }
#pragma warning restore CS8618
    #endregion

    internal Auction(Passenger passenger, Address from, Address to)
    {
        ArgumentNullException.ThrowIfNull(passenger);
        ArgumentNullException.ThrowIfNull(from);
        ArgumentNullException.ThrowIfNull(to);

        this.Passenger = passenger;
        this.From = from;
        this.To = to;
    }

    public int Id { get; private init; }

    public Passenger Passenger { get; private set; }

    public Address From { get; private set; }

    public Address To { get; private set; }

    public IReadOnlyDictionary<Guid, Candidate> CandidatesByDriverId => candidates.ToDictionary(c => c.Driver.Id);

    public void AddCandidate(Driver driver, decimal cost)
    {
        ArgumentNullException.ThrowIfNull(driver);

        if (CandidatesByDriverId.ContainsKey(driver.Id))
        {
            throw new InvalidOperationException("Driver already exist into auction.");
        }

        candidates.Add(Candidate.Create(driver, cost));
    }

    public Journey CreateJourney(Guid userId)
    {
        if (!CandidatesByDriverId.ContainsKey(userId))
        {
            throw new InvalidOperationException("Candidate not mapped to auction.");
        }

        var candidate = CandidatesByDriverId[userId];
        return new Journey(Passenger, candidate.Driver, From, To, candidate.Cost);
    }

    public Journey CreateJourneyWithChipestCandidate()
    {
        if (CandidatesByDriverId.Count == 0)
        {
            throw new InvalidOperationException("You should have at least one candidate.");
        }

        var chipestCandidate = candidates.MinBy(c => c.Cost);
        return CreateJourney(chipestCandidate!.Driver.Id);
    }
}
