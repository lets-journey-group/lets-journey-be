﻿namespace LetsJourney.BE.Business;

public class Journey
{
    #region private constructor
#pragma warning disable CS8618
    private Journey()
    {
    }
#pragma warning restore CS8618
    #endregion

    internal Journey(
        Passenger passenger,
        Driver driver,
        Address from,
        Address to,
        Money cost)
    {
        ArgumentNullException.ThrowIfNull(passenger);
        ArgumentNullException.ThrowIfNull(driver);
        ArgumentNullException.ThrowIfNull(from);
        ArgumentNullException.ThrowIfNull(to);
        ArgumentNullException.ThrowIfNull(cost);

        Passenger = passenger;
        Driver = driver;
        From = from;
        To = to;
        Cost = cost;
    }

    public int Id { get; private init; }

    public Passenger Passenger { get; private set; }

    public Driver Driver { get; private set; }

    public Address From { get; private set; }

    public Address To { get; private set; }

    public Money Cost { get; private set; }
}
