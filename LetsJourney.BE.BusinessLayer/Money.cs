﻿namespace LetsJourney.BE.Business;

public record Money : IComparable<Money>
{
    #region private constructor
    private Money()
    {
    }
    #endregion

    internal Money(decimal value)
    {
        Value = value;
    }

    public decimal Value { get; private init; }

    public int CompareTo(Money? other)
    {
        ArgumentNullException.ThrowIfNull(other);
        return Value.CompareTo(other.Value);
    }
}
