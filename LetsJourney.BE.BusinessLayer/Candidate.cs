﻿namespace LetsJourney.BE.Business;

public record Candidate
{
    public static Candidate Create(Driver driver, decimal cost)
    {
        Money moneyCost = new(cost);
        return new Candidate(driver, moneyCost);
    }

    #region private constructor
#pragma warning disable CS8618
    private Candidate()
    {
    }
#pragma warning restore CS8618
    #endregion

    internal Candidate(Driver driver, Money cost)
    {
        ArgumentNullException.ThrowIfNull(driver);
        ArgumentNullException.ThrowIfNull(cost);

        Driver = driver;
        Cost = cost;
    }

    public Driver Driver { get; private init; }

    public Money Cost { get; private init; }
}
