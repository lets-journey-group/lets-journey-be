﻿using LetsJourney.BE.Api.IntegrationTests.OpenAPIs;
using LetsJourney.BE.Data;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;

namespace LetsJourney.BE.Api.IntegrationTests;

public abstract class TestBase : IClassFixture<CustomWebApplicationFactory<Program>>, IDisposable
{
    protected readonly CustomWebApplicationFactory<Program> Factory;
    protected readonly JourneyContext Context;
    protected readonly HttpClient HttpClient;
    protected readonly ApiClient ApiClient;

    protected TestBase(CustomWebApplicationFactory<Program> factory)
    {
        Factory = factory;
        Context = factory.GetContext();
        HttpClient = factory.CreateClient(new WebApplicationFactoryClientOptions
        {
            AllowAutoRedirect = false
        });

        ApiClient = new OpenAPIs.ApiClient(HttpClient.BaseAddress!.ToString(), HttpClient);
    }

#pragma warning disable CA1816
    public void Dispose()
    {
        ClearDatabase();
    }
#pragma warning restore CA1816

    private void ClearDatabase()
    {
        string getTablesQuery = @"SELECT name 
                                      FROM sqlite_schema 
                                      WHERE type ='table' 
                                      AND name NOT LIKE 'sqlite_%';";

        var tableNames = Context.Database
            .SqlQueryRaw<string>(getTablesQuery)
            .ToList();

        foreach (var tableName in tableNames)
        {
            Context.Database.ExecuteSqlRaw(string.Format("DELETE FROM {0}", tableName));
        }

        Context.SaveChanges();
    }
}
