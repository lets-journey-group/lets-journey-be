﻿using FluentAssertions;
using LetsJourney.BE.Business;
using Microsoft.EntityFrameworkCore;
using Moq;

namespace LetsJourney.BE.Api.IntegrationTests.TestCases;

public class AuctionControllerTests : TestBase
{
    private const string from = "street";
    private const string from2 = "street2";
    private const string to = "street3";
    private const string to2 = "street4";
    private const double bet = 10d;
    private readonly Guid passengerUserGuid = new("FCDE5C9E-69D1-4193-B569-A4143CD78B2D");
    private readonly Guid driverUserGuid = new("A208F0A4-0766-441B-B070-D67F53D18E7F");
    private readonly Passenger passenger;
    private readonly Driver driver;

    public AuctionControllerTests(
        CustomWebApplicationFactory<Program> factory) : base(factory)
    {
        passenger = Mock.Of<Passenger>(d => d.Id == passengerUserGuid);
        driver = Mock.Of<Driver>(d => d.Id == driverUserGuid);
    }

    #region GetAll

    [Fact]
    public async Task GetAll_WithTwoAuctions_ShouldReturnValidAuctions()
    {
        Guid guid2 = new("FCDE5C9E-69D1-4193-B569-A4143CD78B2E");

        //Arrange
        var passenger2 = Mock.Of<Passenger>(d => d.Id == guid2);
        List<Auction> auctions = new()
        {
            Auction.Create(passenger, from, to),
            Auction.Create(passenger2, from2, to2)
        };

        Context.Auctions.AddRange(auctions);
        await Context.SaveChangesAsync();

        //Act
        var actualAuctions = await ApiClient.GetAllAsync();

        //Assert
        actualAuctions.Should().HaveCount(auctions.Count);

        var first = actualAuctions.First();
        first.From.Street.Should().Be(from);
        first.To.Street.Should().Be(to);
        first.PassengerId.Should().Be(passengerUserGuid);

        var second = actualAuctions.ElementAt(1);
        second.To.Street.Should().Be(to2);
        second.From.Street.Should().Be(from2);
        second.PassengerId.Should().Be(guid2);
    }

    [Fact]
    public async Task GetAll_WithoutAuctions_ShouldReturnEmpty()
    {
        //Act
        var actualAuctions = await ApiClient.GetAllAsync();

        //Assert
        actualAuctions.Should().HaveCount(0);
    }

    #endregion

    #region Get

    [Fact]
    public async Task Get_WithValidAuction_ShouldReturn()
    {
        //Arrange
        Context.Passengers.Add(passenger);
        Context.Auctions.Add(
            Auction.Create(passenger, from, to));
        await Context.SaveChangesAsync();

        var auction = await Context.Auctions.FirstAsync();

        //Act
        var actualAuction = await ApiClient.GetAsync(auction.Id);

        //Assert
        actualAuction.Should().NotBeNull();
        actualAuction.From.Street.Should().Be(from);
        actualAuction.To.Street.Should().Be(to);
        actualAuction.PassengerId.Should().Be(passenger.Id);
    }

    [Fact]
    public async Task Get_WithInvalidId_NotFound404()
    {
        //Act
        Func<Task> fun = async () => await ApiClient.GetAsync(5);

        //Assert
        var exception = await fun.Should().ThrowAsync<OpenAPIs.ApiException>();
        exception.Which.StatusCode.Should().Be(StatusCodes.Status404NotFound);
    }

    #endregion

    #region Create

    [Fact]
    public async Task Create_WithValidAuction_ShouldReturn()
    {
        //Arrange
        Context.Passengers.Add(passenger);
        await Context.SaveChangesAsync();

        var input = new OpenAPIs.Auction
        {
            From = from,
            To = to
        };

        //Act
        var actualAuction = await ApiClient.CreateAsync(input);

        //Assert
        actualAuction.Should().NotBeNull();
        actualAuction.From.Street.Should().Be(from);
        actualAuction.To.Street.Should().Be(to);
        actualAuction.PassengerId.Should().Be(passengerUserGuid);
    }

    [Fact]
    public async Task Create_WithoutPassenger_BadRequest400()
    {
        //Arrange
        var input = new OpenAPIs.Auction
        {
            From = from,
            To = to
        };

        //Act
        Func<Task> fun = async () => await ApiClient.CreateAsync(input);

        //Assert
        var exception = await fun.Should().ThrowAsync<OpenAPIs.ApiException>();
        exception.Which.StatusCode.Should().Be(StatusCodes.Status400BadRequest);
    }

    #endregion

    #region AddCandidate

    [Fact]
    public async Task AddCandidate_WithValidAuction_ShouldReturnAuctionWithCandidate()
    {
        //Arrange
        Context.Passengers.Add(passenger);
        Context.Drivers.Add(driver);

        Auction auction = Auction.Create(passenger, from, to);
        Context.Auctions.Add(auction);

        await Context.SaveChangesAsync();

        //Act
        var actualAuction = await ApiClient.AddCandidateAsync(auction.Id, bet);

        //Assert
        actualAuction.Should().NotBeNull();
        actualAuction.From.Street.Should().Be(from);
        actualAuction.To.Street.Should().Be(to);
        actualAuction.PassengerId.Should().Be(passengerUserGuid);

        actualAuction.CandidateIdCollection.Should().HaveCount(1);

        var actualCandidate = actualAuction.CandidateIdCollection.First();
        actualCandidate.Should().Be(driverUserGuid.ToString());
        //TODO: check bet
    }

    [Fact]
    public async Task AddCandidate_WithValidAuction_GetAuctionMethodShouldWork()
    {
        //Arrange
        Context.Passengers.Add(passenger);
        Context.Drivers.Add(driver);

        Auction auction = Auction.Create(passenger, from, to);
        Context.Auctions.Add(auction);

        await Context.SaveChangesAsync();

        //Act
        _ = await ApiClient.AddCandidateAsync(auction.Id, bet);
        var actualAuction = await ApiClient.GetAsync(auction.Id);

        //Assert
        actualAuction.Should().NotBeNull();
        actualAuction.From.Street.Should().Be(from);
        actualAuction.To.Street.Should().Be(to);
        actualAuction.PassengerId.Should().Be(passengerUserGuid);

        actualAuction.CandidateIdCollection.Should().HaveCount(1);

        var actualCandidate = actualAuction.CandidateIdCollection.First();
        actualCandidate.Should().Be(driverUserGuid.ToString());
        //TODO: check bet
    }

    [Fact]
    public async Task AddCandidate_WithoutAuction_BadRequest400()
    {
        //Arrange
        Context.Drivers.Add(driver);
        await Context.SaveChangesAsync();

        const int auctionId = 5;

        //Act
        Func<Task> fun = async () => await ApiClient.AddCandidateAsync(auctionId, bet);

        //Assert
        var exception = await fun.Should().ThrowAsync<OpenAPIs.ApiException>();
        exception.Which.StatusCode.Should().Be(StatusCodes.Status400BadRequest);
    }

    [Fact]
    public async Task AddCandidate_WithoutDriver_BadRequest400()
    {
        //Arrange
        Context.Passengers.Add(passenger);

        Auction auction = Auction.Create(passenger, from, to);
        Context.Auctions.Add(auction);

        await Context.SaveChangesAsync();

        //Act
        Func<Task> fun = async () => await ApiClient.AddCandidateAsync(auction.Id, bet);

        //Assert
        var exception = await fun.Should().ThrowAsync<OpenAPIs.ApiException>();
        exception.Which.StatusCode.Should().Be(StatusCodes.Status400BadRequest);
    }

    #endregion
}
